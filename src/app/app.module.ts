import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  ShowOnDirtyErrorStateMatcher,
  MatCheckboxModule,
  ErrorStateMatcher,
  MatButtonModule,
  MatInputModule,
  MatCardModule,
} from '@angular/material';

import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {PageOfflineComponent} from './components/page-offline/page-offline.component';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app.routing';
import {AppComponent} from './app.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    PageOfflineComponent,
    LoginComponent,
    HomeComponent,
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    HttpClientModule,
    AppRoutingModule,
    AppRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    BrowserModule,
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  providers: [
    {
      provide: ErrorStateMatcher,
      useClass: ShowOnDirtyErrorStateMatcher
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
