import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {PageOfflineComponent} from './components/page-offline/page-offline.component';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {path: 'offline', component: PageOfflineComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {

}
