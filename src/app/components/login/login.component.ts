import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public rForm: FormGroup;

  constructor(private _formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.rForm = this._formBuilder.group({
      email: [null, Validators.email],
      password: [null, Validators.required],
    });
  }

  public login(data): void {
      window.alert('login');
      console.log(data);
  }

}
